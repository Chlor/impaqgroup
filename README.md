# Impaqgroup

## Description

Simple edit/remove app

## Dependencies

* nodejs ^4.1.2
* npm ^2.14.4

## Install

```
npm install
```

## Test

```
npm test
```

## Run

```
npm start
```

... and visit localhost:8080