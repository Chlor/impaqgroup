'use strict'

let gulp       = require('gulp'),
    source     = require('vinyl-source-stream'),
    browserify = require('browserify'),
    gulpConcat = require('gulp-concat'),
    gulpFilter = require('gulp-filter'),
    sourcemaps = require('gulp-sourcemaps'),
    watchify   = require('watchify'),
    buffer     = require('vinyl-buffer'),
    gutil      = require('gulp-util'),
    babelify   = require('babelify')

const BOOTSTRAP = './node_modules/bootstrap-css-only',
      DIST      = './public/dist'

gulp.task('css', () =>
    gulp.src(`${BOOTSTRAP}/css/*.min.css`)
      .pipe(gulpConcat('vendor.css'))
      .pipe(gulp.dest(`${DIST}/css`))
)

gulp.task('fonts', () =>
    gulp.src(`${BOOTSTRAP}/fonts/*`)
      .pipe(gulp.dest(`${DIST}/fonts`))
)

function prepareBrowserify(watch) {
  let customOpts = {
        entries: ['./client/app.js'],
        debug: true
      },
      opts       = Object.assign({}, watchify.args, customOpts),
      b          = browserify(opts)

  if (watch) {
    b = watchify(b)
    b.on('update', () => bundle(b))
  }

  b.transform([babelify.configure({
    extensions: ['.js'],
    experimental: true,
    optional: ['runtime']
  })])

  b.on('log', gutil.log)

  bundle(b)
}

gulp.task('browserify', () => prepareBrowserify(false))

gulp.task('watch', () => prepareBrowserify(true))

function bundle(b) {
  return b.bundle()
    .on('error', function (err) {
      error.call(this, err)
    })
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadmaps: true}))
    .pipe(sourcemaps.write(''))
    .pipe(gulp.dest(DIST))
}

function error(err) {
  gutil.log(err)
  this.emit('end')
}
gulp.task('build', ['css', 'fonts', 'browserify'])

gulp.task('default', ['watch'])
