import angular from 'angular'
import MockApi from './lib/MockApi'

export default function mockBackendLoader(App) {
  if (window.jasmine) {
    return null
  }
  require('angular-mocks')
  MockApi.seed()
  App.config($provide => {
      $provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator)
    })
    .run(mockBackend)
}

function extractIdFromUrl(url) {
  return +url.split('/').pop()
}

function mockBackend($httpBackend) {
  httpBackend($httpBackend)
  $httpBackend.whenGET(/.*/).passThrough()
}

export function httpBackend($httpBackend) {
  $httpBackend.whenGET(/\/findall$/)
    .respond(() =>[200, MockApi.findAll(), {}])

  $httpBackend.whenGET(/\/find\/\d+$/)
    .respond((method, url) => {
      return [200, MockApi.find(extractIdFromUrl(url)) || {}, {}]
    })

  $httpBackend.whenPOST(/\/remove\/\d+$/)
    .respond((method, url, data) => {
      let result = MockApi.remove(extractIdFromUrl(url), JSON.parse(data))
      return [200, result , {}]
    })

  $httpBackend.whenPOST(/\/edit\/\d+$/)
    .respond((method, url, data) => {
      let result = MockApi.edit(extractIdFromUrl(url), JSON.parse(data))
      return [200, result, {}]
    })
}

if (window.jasmine) {
  mockBackend = function () {}
}

