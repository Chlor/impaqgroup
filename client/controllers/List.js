import {copy} from 'angular'
import Utils from '../lib/Utils'

const SERVICES = new WeakMap(),
      TEMP     = new WeakMap()

export default class ListCtrl {

  /**
   * @constructor
   * @description
   * 1. set references to injected services in weak map
   * 2. create User objects @ users
   * 3. set initial state of flags
   * 3a. selected is used by #toggleAll
   * 3b. editMode is user for #editAll
   * 4. listen for Api events via User#listenToUpdates (proxy)
   * @param {User} User
   * @param {Object[]} users
   */
  constructor(User, users) {
    SERVICES.set(this, {User, users})
    this.users    = users.map(user => new User(user))
    this.selected = false
    this.editMode = false

    User.listenToUpdates(this.checkIfStillEditing, this)
  }

  /**
   * @description remove from local users collection upon successful
   * remove api call
   * @param {User} - user
   * @param {Number} idx - index of user (from angular iterator)
   */
  remove(user, idx) {
    user.remove().then(() => this.users.splice(idx, 1))
  }

  /**
   * @description set edit flag for all users (based on this.selected flag)
   */
  toggle() {
    this.users.forEach(user => user.edit = this.selected)
  }

  /**
   * @description switch value of user.edit
   * @param user
   */
  toggleOne(user) {
    user.edit = !user.edit
  }

  /**
   * @description
   * 1. snapshot current user state via User constructor. @see #cancel
   * 2. set showEdit flag to true (user is editable)
   * @param {User} user
   */
  edit(user) {
    let {User} = SERVICES.get(this)
    TEMP.set(user, new User(user, {edit: user.edit}))
    user.showEdit = true
  }

  /**
   * @description call #edit for all users with #edit flag set to true,
   * set this.editMode to true
   */
  editAll() {
    let toEdit = this.users.filter(({edit}) => edit)
    if (toEdit.length > 0) {
      this.editMode = true
      toEdit.forEach(this.edit.bind(this))
    }
  }

  /**
   * @proxy
   * @param {User} user
   */
  save(user) {
    user.save()
  }

  /**
   * @description call user#save for all users that are in editMode (showEdit)
   * @proxy
   */
  saveAll() {
    this.users.forEach(user => {
      if (user.showEdit) {
        user.save()
      }
    })
  }

  /**
   * @description
   * 1. restore user state from before edit
   * 2. delete state
   * 3. set user.showEdit to false
   * 4. check if there are any other users are being edited
   * @param {User} user
   */
  cancel(user) {
    user.revert(TEMP.get(user))
    TEMP.delete(user)
    user.showEdit = false
    this.checkIfStillEditing()
  }

  /**
   * @proxy
   */
  cancelAll() {
    this.users.forEach(this.cancel.bind(this))
  }

  /**
   * @description global edit needs to be turned off, when no users are
   * being edited, that`s what this method does
   */
  checkIfStillEditing() {
    if (this.users.every(({showEdit}) => !showEdit)) {
      this.editMode = false
    }
  }
}
