'use strict'

const PORT = 8080

/**
 * es6 imports don`t work in node 4.1.2 without transpiler and as this
 * server only does one thing, I felt it`s and overkill to transpile it.
 */
let path         = require('path'),
    fs           = require('fs'),
    promisifyAll = require('bluebird').promisifyAll,
    koa          = require('koa'),
    koaStatic    = require('koa-static'),
    app          = koa()

promisifyAll(fs)

app.use(koaStatic(path.resolve('public')))

app.use(function* () {
  this.body = yield fs.readFileAsync('./public/index.html', {encoding: 'utf8'})
})

app.listen(PORT, () => console.log(`App listening on ${PORT}`))
