function router($urlRouterProvider, $stateProvider, $locationProvider) {
  $stateProvider
    .state('list', {
      url: '/list',
      resolve: {
        users(User) {
          return User.findAll()
        }
      },
      views: {
        'main': {
          templateUrl: 'views/list.html',
          controller: 'ListCtrl',
          controllerAs: 'ctrl'
        }
      }
    })
  $urlRouterProvider.otherwise('/list')
  $locationProvider.html5Mode(true)
}

export default router
