import {copy} from 'angular'
import Utils from '../lib/Utils'

const ADDITIONAL_KEYS = new Set(['edit', 'showEdit']),
      SERVICES        = new WeakMap()

class User {
  /**
   * @constructor
   * @desription
   * 1. break the reference
   * 2. add special, non-enumerable props eg. some flags
   * @param {Object} user - raw data
   * @param {Object} params - these will override default configuration for
   * non-enumerable props, eg. when user is currently being edited
   */
  constructor(user, params) {
    if (params) {
      ADDITIONAL_KEYS.add(Reflect.ownKeys(params))
    }
    Object.assign(this, angular.copy(user)) // break the reference
    Utils.defineProps(this, Object.assign({edit: false, showEdit: false}, params))
  }

  /**
   * @proxy
   * @see Api#edit
   * @returns {Promise}
   */
  save() {
    this.serialize()
    return SERVICES.get(User).Api.edit(this)
  }

  /**
   * @proxy
   * @see Api#remove
   * @returns {Promise}
   */
  remove() {
    this.serialize()
    return SERVICES.get(User).Api.remove(this)
  }

  /**
   * @description remove all additional props, return raw user (with methods)
   * since it json serialized anyway, we don`t have to worry about them
   */
  serialize() {
    ADDITIONAL_KEYS.forEach(key => delete this[key])
  }

  /**
   * @description restore some previous state without breaking a binding
   * (useful for angular 2way data binding or FSMs)
   * @param {User} oldUser
   */
  revert(oldUser) {
    if (oldUser instanceof User) {
      Reflect.ownKeys(oldUser).forEach(key => this[key] = oldUser[key])
    }
  }

  /**
   * @static
   * @proxy
   * @see Api#addChangeListener
   * @param fn
   * @param ctx
   */
  static listenToUpdates(fn, ctx) {
    SERVICES.get(User).Api.addChangeListener(fn, ctx)
  }

  /**
   * @static
   * @proxy
   * @see Api#findAll
   * @returns {Promise}
   */
  static findAll() {
    return SERVICES.get(User).Api.findAll()
  }

  /**
   * @static
   * @proxy
   * @see Api#find
   * @returns {Promise}
   */
  static find(id) {
    return SERVICES.get(User).Api.find(id)
  }
}

/**
 * @description expose injectable class that exposes @User
 * @api public
 */
class UserFactory {
  constructor() {
    return User
  }

  /**
   * @description intercept angular dependencies, return ctor exposing @User
   * instance
   * @api public
   * @param Api
   */
  static create(Api) {
    SERVICES.set(User, {Api})
    return new UserFactory()
  }
}

UserFactory.$inject = ['Api']

export default UserFactory.create
