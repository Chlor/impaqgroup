import sinonAsPromised from 'sinon-as-promised'
import MockApi from '../../client/lib/MockApi'
import {httpBackend} from '../../client/mock-backend'
import TEST_USERS from '../test-users.json'

import ListCtrl from '../../client/controllers/List'

let $q, $timeout, $httpBackend, User, flush, controller

describe('List Controller', () => {

  beforeEach(angular.mock.module('App'))

  beforeEach(() => {
    inject((_$q_, _$timeout_, _$httpBackend_, _User_) => {
      $q           = _$q_
      $timeout     = _$timeout_
      $httpBackend = _$httpBackend_
      sinonAsPromised($q)

      User = _User_
    })
  })

  beforeEach(() => {
    MockApi.clear()
    MockApi.seed(TEST_USERS)
    httpBackend($httpBackend)
    flush = () => {
      $httpBackend.flush()
      $timeout.flush()
    }
  })


  beforeEach(() => {
    controller = new ListCtrl(User, TEST_USERS)
  })

  it('should create User objects from users passed to constructor', () => {
    assert.isArray(controller.users)
    assert.equal(controller.users.length, 3)
    controller.users.forEach(user => assert.instanceOf(user, User))
  })

  describe('#remove', () => {
    it('should remove users from controller.users list', () => {
      controller.remove(controller.users[0], 0)
      flush()
      assert.equal(controller.users.length, 2)
    })
  })

  describe('#toggle', () => {
    it('should set @edit for all users', () => {
      controller.selected = true
      controller.toggle()
      controller.users.forEach(user => assert.isTrue(user.edit))
    })
  })

  describe('#toggleOne', () => {
    it('should toggle @edit for one user', () => {
      controller.toggleOne(controller.users[0])
      assert.isTrue(controller.users[0].edit)
    })
  })

  describe('#edit', () => {
    it('should set @showEdit for one user', () => {
      controller.edit(controller.users[0])
      assert.isTrue(controller.users[0].showEdit)
    })
  })

  describe('#editAll', () => {
    it('should set @showEdit for all users with active edit flag', () => {
      controller.users[1].edit = true
      controller.users[2].edit = true
      controller.editAll()
      assert.isTrue(controller.users[1].showEdit)
      assert.isTrue(controller.users[2].showEdit)
    })
  })

  /**
   * #save and #saveAll methods are proxies
   */

  describe('#cancel', () => {
    it('should revert user to an original state', () => {
      let user  = controller.users[0]
      controller.edit(user)
      user.name = 'test777'
      controller.cancel(user)
      assert.equal(user.name, 'test')
    })
  })

  /**
   * #cancelAll is a proxy
   */

  describe('#checkIfStillEditing', () => {
    it('should set @editMode to false (because all @showEdit flags are false', () => {
      controller.editMode = true
      controller.checkIfStillEditing()
      assert.isFalse(controller.editMode)
    })
  })

})
