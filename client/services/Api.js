import {EventEmitter} from 'events'

const SERVICES        = new WeakMap(),
      API_URL         = 'http://users.impaqgroup.com',
      EVENT_LISTENERS = new WeakMap()

export default class Api extends EventEmitter {

  /**
   * @description - extend event emitter - provide ability to listen for changes
   * (edit/remove) - just like flux arch
   * @constructor
   */
  constructor($http, $q) {
    super()
    SERVICES.set(this, {$http, $q})
  }

  /**
   * @returns {Promise}
   */
  findAll() {
    let {$http, $q} = SERVICES.get(this)
    return new $q((resolve, reject) =>
        $http.get(`${API_URL}/findall`).success(resolve).error(reject)
    )
  }

  /**
   * @param id
   * @returns {Promise}
   */
  find(id) {
    let {$http, $q} = SERVICES.get(this)
    return new $q((resolve, reject) =>
        $http.get(`${API_URL}/find/${id}`).success(resolve).error(reject)
    )
  }

  /**
   * @notify
   * @param user
   * @returns {Promise}
   */
  edit(user) {
    let {$http, $q} = SERVICES.get(this)
    return new $q((resolve, reject) =>
        $http.post(`${API_URL}/edit/${user.id}`, user)
          .success((res) => {
            this.notifyChange()
            resolve(res)
          })
          .error(reject)
    )
  }

  /**
   * @notify
   * @param {User} user
   * @returns {Promise}
   */
  remove(user) {
    let {$http, $q} = SERVICES.get(this)
    return new $q((resolve, reject) =>
        $http.post(`${API_URL}/remove/${user.id}`, user)
          .success((res) => {
            this.notifyChange()
            resolve(res)
          })
          .error(reject)
    )
  }

  /**
   * @description - call this method when change (edit/remove) happens
   */
  notifyChange() {
    this.emit('change')
  }

  /**
   * @description pass in listener function and it`s context (defaults to null)
   * Store references as {<notBound>: <bound>} in order to be able to remove
   * listener in the future
   * @param {Function} fn
   * @param {any} ctx - context that the function will be bound to
   */
  addChangeListener(fn, ctx = null) {
    if (!EVENT_LISTENERS.get(fn)) {
      EVENT_LISTENERS.set(fn, fn.bind(ctx))
      this.on('change', EVENT_LISTENERS.get(fn))
    }
  }
}
