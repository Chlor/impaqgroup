export default class Utils {
  static defineProps(target, props) {
    Reflect.ownKeys(props).forEach(key => {
      Object.defineProperty(target, key, {
        value: props[key],
        writable: true,
        configurable: true
      })
    })
  }
}
