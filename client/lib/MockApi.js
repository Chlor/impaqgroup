import model from './users.json'
const USERS = new WeakMap()

export default class MockApi {

  /**
   * use either users or model for data mock
   * @param users
   * @returns {MockApi}
   */
  static seed(users = null) {
    USERS.set(this, users ? users.concat() : model.concat())
    return this
  }

  /**
   * clear data struct
   */
  static clear() {
    USERS.set(this, null)
  }

  /**
   * @returns {[]}
   */
  static findAll() {
    return USERS.get(this) || []
  }

  /**
   * @param {number} id
   * @returns {Object|undefined}
   */
  static find(id) {
    return USERS.get(this).filter(({id: userId}) => id === userId).pop()
  }

  /**
   * @description
   * 1. remove user id, it`s not editable
   * 2. find user to update and replace it with the new one
   * 2a. copy id
   * 2b. set idx, indicate, that users was found
   * 3. save new user
   * 4. return new user or null if it wasn`t updated
   * @param {number} id
   * @param {Object} user
   * @returns {Object|null}
   */
  static edit(id, user) {
    delete user.id
    let users = USERS.get(this),
        idx   = -1
    users     = users.map((oUser, index) => {
      let oId = oUser.id
      if (id === oId) {
        oUser    = user
        oUser.id = oId
        idx      = index
      }
      return oUser
    })
    USERS.set(this, users)
    return !!~idx ? users[idx] : null
  }

  /**
   * @param {number} id
   * @param {Object} user - I don`t really know what to do with this here...
   * @returns {Object|boolean}
   */
  static remove(id, user) {
    let users   = USERS.get(this),
        idx     = users.findIndex(({id: userId}) => id === userId),
        removed = null
    if (!!~idx) {
      removed = users.splice(idx, 1).pop() // splice() always returns array
      USERS.set(users)
    }
    return removed
  }
}
