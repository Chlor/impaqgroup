import angular from 'angular'
import uiRouter from 'angular-ui-router'
import uiBootstrap from 'angular-ui-bootstrap'

import router from './router'
import services from './services/index'
import factories from './factories'
import controllers from './controllers/index'
import filters from './filters/index'

let App = angular.module('App', [uiRouter, uiBootstrap])
  .config(router)
  .service(services)
  .factory(factories)
  .controller(controllers)
  .filter(filters)

import mockBackendLoader from './mock-backend'
mockBackendLoader(App)


