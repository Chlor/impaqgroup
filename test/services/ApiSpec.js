import sinonAsPromised from 'sinon-as-promised'
import MockApi from '../../client/lib/MockApi'
import {httpBackend} from '../../client/mock-backend'
import TEST_USERS from '../test-users.json'

let Api, $q, $timeout, $httpBackend, flush

describe('Api', () => {

  beforeEach(angular.mock.module('App'))

  beforeEach(() => {
    inject((_$q_, _$timeout_, _Api_, _$httpBackend_) => {
      $q           = _$q_
      $timeout     = _$timeout_
      Api          = _Api_
      $httpBackend = _$httpBackend_
      sinonAsPromised($q)
    })
  })

  beforeEach(() => {
    MockApi.clear()
    MockApi.seed(TEST_USERS)
    httpBackend($httpBackend)
    flush = () => {
      $httpBackend.flush()
      $timeout.flush()
    }
  })

  describe('#findAll', () => {

    it('should fetch users', () => {
      Api.findAll().then(users => {
        assert.equal(users.length, 3)
        users.forEach(user => assert.isDefined(user.id))
      })
      flush()
    })

    it('should return empty array, if there are no results', () => {
      MockApi.clear()
      Api.findAll().then(users => {
        assert.isArray(users)
        assert.equal(users.length, 0)
      })
      flush()
    })
  })

  describe('#find', () => {

    it('should find one user by id', () => {
      Api.find(2).then(user => {
        assert.equal(user.id, 2)
        assert.equal(user.name, 'test2')
      })
      flush()
    })

    it('should return empty object if user cannot be found', () => {
      Api.find(4).then(user => {
        assert.equal(Object.keys(user).length, 0)
      })
      flush()
    })
  })

  describe('#edit', () => {

    it('should update user', () => {
      let newUser = {
        id: 1,
        name: 'testEdit'
      }

      Api.edit(newUser).then(user => {
        assert.equal(user.id, 1)
        assert.equal(user.name, 'testEdit')
      })
      Api.find(1).then(user => {
        assert.equal(user.id, 1)
        assert.equal(user.name, 'testEdit')
      })
      flush()
    })

    it('should return null, if user cannot be found', () => {

      Api.edit({id: 5, name: 'test5'})
        .then(user => {
          assert.isNull(user)
        })
      flush()
    })

  })

  describe('#remove', () => {

    it('should remove user by id', () => {
      Api.remove({id: 3})
        .then(({id, name}) => {
          assert.equal(id, 3)
          assert.equal(name, 'test3')
        })
      Api.find(3).then(res => {
        assert.equal(Object.keys(res).length, 0)
      })
      flush()
    })

    it('should return null if user cannot be found', () => {
      Api.remove({id: 4}).then(user => {
        assert.equal(user, null)
      })
      flush()
    })
  })

  describe('#notifyChange', () => {

    it('should notify about change on #update', () => {
      sinon.spy(Api, 'notifyChange')
      Api.edit({id:1, name: 'test777'}).then(() => {
        assert.isTrue(Api.notifyChange.called)
      })
      flush()
    })

    it('should notify about change on #remove', () => {
      sinon.spy(Api, 'notifyChange')
      Api.remove({id:1, name: 'test777'}).then(() => {
        assert.isTrue(Api.notifyChange.called)
      })
      flush()
    })
  })

  describe('#addChangeListener', () => {
    it('should call function on eventEmitter event', () => {
      let cb = sinon.spy()
      Api.addChangeListener(cb)
      Api.edit({id: 1, name: 'test777'}).then(() => {
        assert.isTrue(cb.calledOnce)
      })
      Api.remove({id: 1}).then(() => {
        assert.isTrue(cb.calledTwice)
      })
      flush()
    })
  })

})

