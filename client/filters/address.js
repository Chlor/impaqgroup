export default () => (input = {}) => {
  if (!input) {
    return ''
  }
  let {city = '', street = '', house_number = ''} = input
  if (!(city && street && house_number)) {
    return ''
  }
  return `${city}, ${street} ${house_number}`
}
