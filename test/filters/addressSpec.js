import address from '../../client/filters/address'

let filter

describe('address', () => {

  beforeEach(() => {
    filter = address()
  })

  it('should format input', () => {
    let input = {
      city: 'Warsaw',
      street: 'Wołoska',
      house_number: 22
    }
    assert.equal(filter(input), 'Warsaw, Wołoska 22')
  })

  it('should return empty string for bad values', () => {
    assert.equal(filter({street: 'test'}), '')
    assert.equal(filter([]), '')
    assert.equal(filter(null), '')
    assert.equal(filter(1), '')
  })
})
