import sinonAsPromised from 'sinon-as-promised'
import MockApi from '../../client/lib/MockApi'
import {httpBackend} from '../../client/mock-backend'
import TEST_USERS from '../test-users.json'

let $q, $timeout, $httpBackend, User, newUser, flush

describe('User', () => {

  beforeEach(angular.mock.module('App'))

  beforeEach(() => {
    inject((_$q_, _$timeout_, _$httpBackend_, _User_) => {
      $httpBackend = _$httpBackend_
      $q           = _$q_
      $timeout     = _$timeout_
      sinonAsPromised($q)
      User         = _User_
    })
  })

  beforeEach(() => {
    MockApi.clear()
    MockApi.seed(TEST_USERS)
    httpBackend($httpBackend)
    flush = () => {
      $httpBackend.flush()
      $timeout.flush()
    }
  })

  beforeEach(() => {
    newUser = new User({
      id: 1,
      name: 'test'
    })
  })

  it('should create new User with additional, non-enumerable params', () => {
    let {id, name, edit, showEdit} = newUser

    assert.instanceOf(newUser, User)
    assert.equal(id, 1)
    assert.equal(name, 'test')
    assert.equal(edit, false)
    assert.equal(showEdit, false)
  })

  it('should override default params (edit, showEdit)', () => {
    newUser = new User({id: 1, name: 'test'}, {edit: true, showEdit: true})
    let {edit, showEdit} = newUser
    assert.equal(edit, true)
    assert.equal(showEdit, true)
    assert.isFalse(newUser.propertyIsEnumerable(edit))
    assert.isFalse(newUser.propertyIsEnumerable(showEdit))
  })

  describe('static#findAll', () => {
    it('should fetch all users', () => {
      User.findAll().then((users) => {
        assert.equal(users.length, 3)
      })
      flush()
    })
  })

  describe('static#find', () => {
    it('should fetch user by id', () => {
      User.find(1).then(({id, name}) => {
        assert.equal(id, 1)
        assert.equal(name, 'test')
      })
      flush()
    })
  })

  describe('#serialize', () => {
    it('should remove all special, non-enumerable params', () => {
      newUser.serialize()
      let {edit, showEdit} = newUser
      assert.isUndefined(edit)
      assert.isUndefined(showEdit)
    })
  })

  describe('#revert', () => {
    it('should revert user to previous state', () => {
      let instance = new User(newUser)
      instance.name = 'test2'
      instance.revert(newUser)
      assert.equal(instance.name, 'test')
    })
  })

  describe('#save', () => {
    it('should update via instance', () => {
      newUser.name = 'test2'
      newUser.save().then(() => {
        return User.find(1)
      }).then(({name}) => {
        assert.equal(name, 'test2')
      })
      flush()
    })

    it('shouldn`t allow to change ids', () => {
      newUser.id = 777
      newUser.name = 'test777'
      newUser.save().then(() => {
        return $q.all([User.find(1), User.find(777)])
      }).then(([{id, name}, nonExistent]) => {
        assert.equal(id, 1)
        assert.equal(name, 'test')
        assert.isTrue(Object.keys(nonExistent).length === 0)
      })
      flush()
    })
  })

  describe('#remove', () => {
    it('should remove user (...)', () => {
      newUser.remove().then(res => {
        return User.findAll()
      }).then(res => {
        assert.equal(res.length, 2)
      })
      flush()
    })
  })

})
